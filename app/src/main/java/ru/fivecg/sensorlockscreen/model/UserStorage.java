package ru.fivecg.sensorlockscreen.model;


import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

public final class UserStorage {

    private static final String PREFERENCES_NAME = "user_storage";

    private static final String KEY_NAME = "user_name";

    @NonNull
    public static String getUserName(@NonNull Context context){
        return prefs(context).getString(KEY_NAME, "");
    }

    public static void setUserName(@NonNull Context context, @NonNull String name){
        prefs(context).edit().putString(KEY_NAME, name).commit();
    }

    @NonNull
    private static SharedPreferences prefs(Context context) {
        return context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

}
