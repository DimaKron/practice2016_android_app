package ru.fivecg.sensorlockscreen.protects;


import android.app.Activity;
import android.app.Fragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import java.util.ArrayList;

import me.tatarka.rxloader.RxLoader;
import me.tatarka.rxloader.RxLoaderManager;
import me.tatarka.rxloader.RxLoaderObserver;
import ru.fivecg.sensorlockscreen.R;
import ru.fivecg.sensorlockscreen.model.UserStorage;
import ru.fivecg.sensorlockscreen.model.database.DatabaseHelper;
import ru.fivecg.sensorlockscreen.model.entity.Protect;
import ru.fivecg.sensorlockscreen.model.table.ProtectTable;
import ru.fivecg.sensorlockscreen.model.wrapper.ProtectCursorWrapper;
import ru.fivecg.sensorlockscreen.secret.SecretActivity;
import ru.fivecg.sensorlockscreen.utils.SensorSettings;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

public class AddProtectFragment extends Fragment implements View.OnClickListener, TextWatcher, AdapterView.OnItemSelectedListener {

    private static final int REQUEST_CODE_RECORD = 3321;

    private static final String TAG_UPDATE_LOADER = "update";

    public static final String KEY_USERNAME = "username";

    private static final String KEY_ONE = "one";
    private static final String KEY_TWO = "two";
    private static final String KEY_THREE = "three";

    private EditText protectName;
    private TextView protectActions;
    private Spinner protectType;
    private Button continueButton;

    private boolean isNameGood = false;
    private boolean isActionGood = false;

    private RxLoaderManager loaderManager;
    private RxLoader<SqlBrite.Query> loader;

    public static AddProtectFragment newInstance(String username) {
        Bundle args = new Bundle();
        args.putString(KEY_USERNAME, username);
        AddProtectFragment fragment = new AddProtectFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_add_protect, container, false);

        loaderManager = RxLoaderManager.get(this);

        protectName = (EditText) v.findViewById(R.id.edit_text_name);
        protectName.addTextChangedListener(this);

        protectActions = (TextView) v.findViewById(R.id.text_view_actions);
        protectActions.setOnClickListener(this);

        continueButton = (Button) v.findViewById(R.id.button_continue);
        continueButton.setOnClickListener(this);

        protectType = (Spinner) v.findViewById(R.id.spinner_sensor);
        protectType.setOnItemSelectedListener(this);
        setProtectTypes();

        return v;
    }

    private void setProtectTypes(){
        ArrayList<String> protectNames = new ArrayList<>();
        for(Integer res: SensorSettings.SENSOR_VARIANTS_NAMES){
            protectNames.add(getString(res));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, protectNames);
        protectType.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.text_view_actions:{
                isActionGood = false;
                checkButtonEnabled();
                protectActions.setText(R.string.hint_protect_actions);
                Intent intent = new Intent(getActivity(), ProtectRecordActivity.class);
                intent.putExtra(ProtectRecordActivity.KEY_SENSOR_TYPE, SensorSettings.SENSOR_VARIANTS_TYPES.get(protectType.getSelectedItemPosition()));
                startActivityForResult(intent,REQUEST_CODE_RECORD);
            } break;
            case R.id.button_continue:{
                saveToDatabase();
            } break;
        }

    }

    private void saveToDatabase() {


        loader = loaderManager.create(TAG_UPDATE_LOADER, Observable.create(new Observable.OnSubscribe<SqlBrite.Query>() {
            @Override
            public void call(Subscriber<? super SqlBrite.Query> subscriber) {
                SqlBrite sqlBrite = SqlBrite.create();
                DatabaseHelper helper = new DatabaseHelper(getActivity());
                BriteDatabase briteDatabase = sqlBrite.wrapDatabaseHelper(helper, Schedulers.io());

                ContentValues cv = new ContentValues();
                cv.put(ProtectTable.COLUMN_NAME, protectName.getText().toString().trim());
                cv.put(ProtectTable.COLUMN_SENSOR_TYPE, SensorSettings.SENSOR_VARIANTS_TYPES.get(protectType.getSelectedItemPosition()));
                cv.put(ProtectTable.COLUMN_ACTIONS, protectActions.getText().toString());

                briteDatabase.insert(ProtectTable.TABLE_NAME, cv);

                subscriber.onNext(null);
                subscriber.onCompleted();
            }
        }), new RxLoaderObserver<SqlBrite.Query>() {
            @Override
            public void onNext(SqlBrite.Query value) {
                String username = getArguments().getString(KEY_USERNAME);
                if(!TextUtils.isEmpty(username)){
                    UserStorage.setUserName(getActivity(), username);
                }
                Intent intent = new Intent(getActivity(), SecretActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        loader.start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK){
            switch (requestCode){
                case REQUEST_CODE_RECORD:{
                    String result = data.getFloatExtra(KEY_ONE, 0)+" ";
                    if(protectType.getSelectedItemPosition()==3){
                        result += data.getFloatExtra(KEY_TWO,0)+" ";
                        result += data.getFloatExtra(KEY_THREE,0)+" ";
                    }
                    protectActions.setText(result);
                    isActionGood = true;
                    checkButtonEnabled();
                } break;
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        isNameGood = !TextUtils.isEmpty(charSequence);
        checkButtonEnabled();
    }

    @Override
    public void afterTextChanged(Editable editable) {}

    private void checkButtonEnabled(){
        continueButton.setEnabled(isNameGood && isActionGood);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        isActionGood = false;
        checkButtonEnabled();
        protectActions.setText(R.string.hint_protect_actions);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}
}
