package ru.fivecg.sensorlockscreen.protects;


import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import me.tatarka.rxloader.RxLoader;
import me.tatarka.rxloader.RxLoaderManager;
import me.tatarka.rxloader.RxLoaderObserver;
import ru.fivecg.sensorlockscreen.R;
import ru.fivecg.sensorlockscreen.model.database.DatabaseHelper;
import ru.fivecg.sensorlockscreen.model.entity.Protect;
import ru.fivecg.sensorlockscreen.model.table.ProtectTable;
import ru.fivecg.sensorlockscreen.model.wrapper.ProtectCursorWrapper;
import ru.fivecg.sensorlockscreen.secret.SecretActivity;
import ru.fivecg.sensorlockscreen.utils.RecyclerItemClickListener;
import ru.fivecg.sensorlockscreen.utils.SensorSettings;
import rx.schedulers.Schedulers;

public class ProtectsFragment extends Fragment implements View.OnClickListener, RecyclerView.OnItemTouchListener, RecyclerItemClickListener.OnItemClickListener {

    private static final String TAG_SENSORS_LOADER = "sensors_loader";

    public static final String KEY_USERNAME = "username";

    private static final String KEY_ONE = "one";
    private static final String KEY_TWO = "two";
    private static final String KEY_THREE = "three";

    private static final int REQUEST_CODE_RECORD = 1234;

    private RecyclerView.Adapter adapter;

    private RecyclerView.LayoutManager layoutManager;

    private FrameLayout progressLayout;
    private TextView emptyTextView;

    private RecyclerView recyclerView;

    private FloatingActionButton addButton;

    private List<Protect> protects;

    private boolean isUserAuthorised;

    private int currentPosition = 0;

    private RxLoaderManager loaderManager;
    private RxLoader<SqlBrite.Query> loader;

    public static ProtectsFragment newInstance(String username) {
        Bundle args = new Bundle();
        args.putString(KEY_USERNAME, username);
        ProtectsFragment fragment = new ProtectsFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_sensors, container, false);

        isUserAuthorised = TextUtils.isEmpty(getArguments().getString(KEY_USERNAME));

        loaderManager = RxLoaderManager.get(this);

        layoutManager = new LinearLayoutManager(getActivity());

        progressLayout = (FrameLayout) v.findViewById(R.id.progress_layout);

        emptyTextView = (TextView) v.findViewById(R.id.text_view_empty);

        addButton = (FloatingActionButton) v.findViewById(R.id.button_add);
        addButton.setOnClickListener(this);
        addButton.setVisibility(isUserAuthorised?View.GONE:View.VISIBLE);

        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), this));

        loadProtects();

        return v;
    }

    private void loadProtects(){
        SqlBrite sqlBrite = SqlBrite.create();
        DatabaseHelper helper = new DatabaseHelper(getActivity());
        BriteDatabase briteDatabase =  sqlBrite.wrapDatabaseHelper(helper, Schedulers.io());

        loader = loaderManager.create(TAG_SENSORS_LOADER, briteDatabase.createQuery(ProtectTable.TABLE_NAME, ProtectTable.getSelectAllProtectsQuery()), new RxLoaderObserver<SqlBrite.Query>() {
            @Override
            public void onNext(SqlBrite.Query value) {
                Cursor cursor = value.run();
                protects = new ProtectCursorWrapper(cursor).getSensors();
                Log.d("Main", protects.size()+"");
                adapter = new ProtectsListAdapter(protects);
                recyclerView.setAdapter(adapter);
                progressLayout.setVisibility(View.GONE);
                emptyTextView.setVisibility(protects.isEmpty()?View.VISIBLE:View.GONE);
                if (cursor != null) {
                    cursor.close();
                }
            }
        });

        loader.start();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_add:{
                Intent intent = new Intent(getActivity(), AddProtectActivity.class);
                intent.putExtra(AddProtectActivity.KEY_USERNAME,getArguments().getString(KEY_USERNAME));
                startActivity(intent);
            } break;
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        currentPosition = position;
        Intent intent = new Intent(getActivity(), ProtectRecordActivity.class);
        intent.putExtra(ProtectRecordActivity.KEY_SENSOR_TYPE, Integer.valueOf(protects.get(currentPosition).getSensor()));
        startActivityForResult(intent, REQUEST_CODE_RECORD);
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {}

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {}

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode== Activity.RESULT_OK){
            switch (requestCode){
                case REQUEST_CODE_RECORD:{
                    int index = SensorSettings.SENSOR_VARIANTS_TYPES.indexOf(Integer.valueOf(protects.get(currentPosition).getSensor()));
                    ArrayList<Float> original = new ArrayList<>();
                    ArrayList<Float> current = new ArrayList();
                    if(index==3){
                        String[] originalsStr = protects.get(currentPosition).getActions().split(" ");
                        for (String str: originalsStr){
                            original.add(Float.valueOf(str));
                        }
                        current.add(data.getFloatExtra(KEY_ONE,0));
                        current.add(data.getFloatExtra(KEY_TWO,0));
                        current.add(data.getFloatExtra(KEY_THREE,0));
                    } else {
                        original.add(Float.valueOf(protects.get(currentPosition).getActions()));
                        current.add(data.getFloatExtra(KEY_ONE,0));
                    }
                    if (isTrue(original, current, SensorSettings.SENSOR_VARIANTS_LENGTH.get(index))){
                        Intent intent = new Intent(getActivity(), SecretActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getActivity(),R.string.wrong_password, Toast.LENGTH_LONG).show();
                    }
                } break;
            }
        }
    }

    private boolean isTrue(ArrayList<Float> original, ArrayList<Float> current, float period){
        for (int i=0; i<original.size(); i++){
            float currentPeriod = original.get(i)-current.get(i);
            if (currentPeriod<0) currentPeriod*=-1;
            if (currentPeriod>period){
                return false;
            }
        }
        return true;
    }
}
