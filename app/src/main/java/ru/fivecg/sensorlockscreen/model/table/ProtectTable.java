package ru.fivecg.sensorlockscreen.model.table;


import android.support.annotation.NonNull;

public class ProtectTable {

    public static final String TABLE_NAME = "protects";

    public static final String COLUMN_ID = "id";

    public static final String COLUMN_NAME = "name";

    public static final String COLUMN_SENSOR_TYPE = "sensor";

    public static final String COLUMN_ACTIONS = "actions";

    @NonNull
    public static String getCreateTableQuery() {
        return "CREATE TABLE " + TABLE_NAME + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + COLUMN_NAME + " TEXT NOT NULL, "
                + COLUMN_SENSOR_TYPE + " TEXT NOT NULL, "
                + COLUMN_ACTIONS + " TEXT NOT NULL "
                + ");";
    }

    public static String getSelectAllProtectsQuery(){
        return "SELECT * FROM " + TABLE_NAME + ";";
    }

}
