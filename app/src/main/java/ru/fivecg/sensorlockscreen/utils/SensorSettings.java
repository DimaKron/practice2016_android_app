package ru.fivecg.sensorlockscreen.utils;

import android.hardware.Sensor;

import java.util.ArrayList;

import ru.fivecg.sensorlockscreen.R;

public class SensorSettings {

    public static final ArrayList<Integer> SENSOR_VARIANTS_NAMES = new ArrayList<Integer>(){{
        add(R.string.sensor_tem);
        add(R.string.sensor_pre);
        add(R.string.sensor_lig);
        add(R.string.sensor_rot);
    }};

    public static final ArrayList<Integer> SENSOR_VARIANTS_TYPES = new ArrayList<Integer>(){{
        add(android.hardware.Sensor.TYPE_AMBIENT_TEMPERATURE);
        add(Sensor.TYPE_PRESSURE);
        add(android.hardware.Sensor.TYPE_LIGHT);
        add(android.hardware.Sensor.TYPE_ROTATION_VECTOR);
    }};

    public static final ArrayList<Float> SENSOR_VARIANTS_LENGTH = new ArrayList<Float>(){{
        add(1.0f);
        add(1.0f);
        add(3.0f);
        add(0.01f);
    }};

}
