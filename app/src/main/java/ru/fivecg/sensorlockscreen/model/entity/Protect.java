package ru.fivecg.sensorlockscreen.model.entity;


public class Protect {

    private long id;

    private String name;

    private String sensor;

    private String actions;

    public Protect(long id, String name, String sensor, String actions) {
        this.id = id;
        this.name = name;
        this.sensor = sensor;
        this.actions = actions;
    }

    public Protect(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSensor() {
        return sensor;
    }

    public void setSensor(String sensor) {
        this.sensor = sensor;
    }

    public String getActions() {
        return actions;
    }

    public void setActions(String actions) {
        this.actions = actions;
    }
}
