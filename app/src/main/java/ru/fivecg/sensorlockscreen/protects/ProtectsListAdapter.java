package ru.fivecg.sensorlockscreen.protects;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.fivecg.sensorlockscreen.R;
import ru.fivecg.sensorlockscreen.model.entity.Protect;

public class ProtectsListAdapter extends RecyclerView.Adapter<ProtectsListAdapter.ViewHolder> {

    private List<Protect> protects;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;

        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.text_view_name);
        }
    }

    public ProtectsListAdapter(@NonNull List<Protect> protects) {
        this.protects = protects;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.protects_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Protect protect = protects.get(position);
        holder.name.setText(protect.getName());
    }

    @Override
    public int getItemCount() {
        return protects.size();
    }

}
