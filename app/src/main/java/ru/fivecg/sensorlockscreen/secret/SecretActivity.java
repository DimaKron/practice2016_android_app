package ru.fivecg.sensorlockscreen.secret;

import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ru.fivecg.sensorlockscreen.utils.FragmentHostActivity;

public class SecretActivity extends FragmentHostActivity {

    @Override
    protected Fragment createFragment() {
        return SecretFragment.newInstance();
    }
}
