package ru.fivecg.sensorlockscreen.protects;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import ru.fivecg.sensorlockscreen.R;
import ru.fivecg.sensorlockscreen.utils.SensorSettings;

public class ProtectRecordFragment extends Fragment implements SensorEventListener, View.OnClickListener {

    private static final String KEY_ONE = "one";
    private static final String KEY_TWO = "two";
    private static final String KEY_THREE = "three";

    public static final String KEY_SENSOR_TYPE = "sensor_type";

    private TextView valueOne, valueTwo, valueThree;
    private TextView valueNameOne, valueNameTwo, valueNameThree;
    private TextView sensorName;
    private SensorManager sensorManager;
    private Button okButton;
    private TextView noSensor;

    private int sensorType;

    public static ProtectRecordFragment newInstance(int sensorType) {
        Bundle args = new Bundle();
        args.putInt(KEY_SENSOR_TYPE, sensorType);
        ProtectRecordFragment fragment = new ProtectRecordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_protect_record, container, false);

        sensorType = getArguments().getInt(KEY_SENSOR_TYPE);

        noSensor = (TextView) v.findViewById(R.id.text_view_no_sensor);

        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        Sensor sensor = sensorManager.getDefaultSensor(sensorType);
        if(!sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL)){
            noSensor.setVisibility(View.VISIBLE);
        }

        sensorName = (TextView) v.findViewById(R.id.text_view_name);
        sensorName.setText(SensorSettings.SENSOR_VARIANTS_NAMES.get(SensorSettings.SENSOR_VARIANTS_TYPES.indexOf(sensorType)));

        okButton = (Button) v.findViewById(R.id.button_select);
        okButton.setOnClickListener(this);

        valueOne = (TextView) v.findViewById(R.id.text_view_value1);
        valueTwo = (TextView) v.findViewById(R.id.text_view_value2);
        valueThree = (TextView) v.findViewById(R.id.text_view_value3);

        valueNameOne = (TextView) v.findViewById(R.id.text_view_value_name1);
        valueNameTwo = (TextView) v.findViewById(R.id.text_view_value_name2);
        valueNameThree = (TextView) v.findViewById(R.id.text_view_value_name3);

        if(sensorType== SensorSettings.SENSOR_VARIANTS_TYPES.get(3)){
            valueNameOne.setText(R.string.ox);
            valueNameTwo.setText(R.string.oy);
            valueNameThree.setText(R.string.oz);
        } else {
            valueNameOne.setText(R.string.value);
            valueNameTwo.setVisibility(View.GONE);
            valueNameThree.setVisibility(View.GONE);
            valueTwo.setVisibility(View.GONE);
            valueThree.setVisibility(View.GONE);
        }

        return v;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        valueOne.setText(String.valueOf(sensorEvent.values[0]));
        if(sensorType== SensorSettings.SENSOR_VARIANTS_TYPES.get(3)){
            valueTwo.setText(String.valueOf(sensorEvent.values[1]));
            valueThree.setText(String.valueOf(sensorEvent.values[2]));
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {}


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_select: {
                Intent data = new Intent();
                data.putExtra(KEY_ONE, Float.parseFloat(valueOne.getText().toString()));
                if(sensorType== SensorSettings.SENSOR_VARIANTS_TYPES.get(3)){
                    data.putExtra(KEY_TWO, Float.parseFloat(valueTwo.getText().toString()));
                    data.putExtra(KEY_THREE, Float.parseFloat(valueThree.getText().toString()));
                }
                getActivity().setResult(Activity.RESULT_OK, data);
                getActivity().finish();
            } break;
        }
    }
}
