package ru.fivecg.sensorlockscreen.model.wrapper;

import android.database.Cursor;
import android.database.CursorWrapper;

import java.util.ArrayList;
import java.util.List;

import ru.fivecg.sensorlockscreen.model.entity.Protect;
import ru.fivecg.sensorlockscreen.model.table.ProtectTable;


public class ProtectCursorWrapper extends CursorWrapper {

    public ProtectCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Protect getSensor(){
        if(!isBeforeFirst() && !isAfterLast()) {
            int id = getInt(getColumnIndex(ProtectTable.COLUMN_ID));
            String name = getString(getColumnIndex(ProtectTable.COLUMN_NAME));
            String sensor = getString(getColumnIndex(ProtectTable.COLUMN_SENSOR_TYPE));
            String actions = getString(getColumnIndex(ProtectTable.COLUMN_ACTIONS));
            return new Protect(id, name, sensor, actions);
        } else {
            return null;
        }
    }

    public List<Protect> getSensors(){
        ArrayList<Protect> protects = new ArrayList<>();
        moveToFirst();
        while (!isBeforeFirst() && !isAfterLast()){
            protects.add(getSensor());
            moveToNext();
        }
        return protects;
    }
}
