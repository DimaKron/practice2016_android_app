package ru.fivecg.sensorlockscreen.start;

import android.app.Fragment;

import ru.fivecg.sensorlockscreen.utils.FragmentHostActivity;

public class StartActivity extends FragmentHostActivity {


    @Override
    protected Fragment createFragment() {
        return StartFragment.newInstance();
    }

}
