package ru.fivecg.sensorlockscreen.utils;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ru.fivecg.sensorlockscreen.R;


public abstract class FragmentHostActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_host);

        FragmentManager fm = getFragmentManager();
        if(fm.findFragmentById(R.id.fragment_container)== null){
            fm.beginTransaction()
                    .add(R.id.fragment_container, createFragment())
                    .commit();
        }

    }

    protected abstract Fragment createFragment();
}
