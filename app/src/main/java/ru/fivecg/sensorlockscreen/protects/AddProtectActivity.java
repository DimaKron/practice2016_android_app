package ru.fivecg.sensorlockscreen.protects;

import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ru.fivecg.sensorlockscreen.utils.FragmentHostActivity;

public class AddProtectActivity extends FragmentHostActivity {

    public static final String KEY_USERNAME = "username";

    @Override
    protected Fragment createFragment() {
        return AddProtectFragment.newInstance(getIntent().getStringExtra(KEY_USERNAME));
    }

}
