package ru.fivecg.sensorlockscreen.protects;

import android.app.Fragment;

import ru.fivecg.sensorlockscreen.utils.FragmentHostActivity;

public class ProtectsActivity extends FragmentHostActivity {

    public static final String KEY_USERNAME = "username";

    @Override
    protected Fragment createFragment() {
        return ProtectsFragment.newInstance(getIntent().getStringExtra(KEY_USERNAME));
    }
}
