package ru.fivecg.sensorlockscreen.secret;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.fivecg.sensorlockscreen.R;

public class SecretFragment extends Fragment {

    public static SecretFragment newInstance() {
        return new SecretFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_secret, container, false);
    }

}
