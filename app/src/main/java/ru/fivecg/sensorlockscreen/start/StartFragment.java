package ru.fivecg.sensorlockscreen.start;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import ru.fivecg.sensorlockscreen.R;
import ru.fivecg.sensorlockscreen.protects.ProtectsActivity;
import ru.fivecg.sensorlockscreen.model.UserStorage;
import ru.fivecg.sensorlockscreen.protects.ProtectsFragment;


public class StartFragment extends Fragment implements View.OnClickListener{

    private TextView nameTextView, needNameTextView;
    private EditText nameEditText;
    private Button continueButton;

    private boolean isAuthorised;

    public static StartFragment newInstance() {
        return new StartFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_start, container, false);

        String nameStr = UserStorage.getUserName(getActivity());
        isAuthorised = !TextUtils.isEmpty(nameStr);

        nameTextView = (TextView) view.findViewById(R.id.text_view_name);

        needNameTextView = (TextView) view.findViewById(R.id.text_view_enter_name);

        nameEditText = (EditText) view.findViewById(R.id.edit_text_name);

        continueButton = (Button) view.findViewById(R.id.button_go);
        continueButton.setOnClickListener(this);

        if(isAuthorised){
            nameTextView.setText(nameStr);
            needNameTextView.setVisibility(View.GONE);
            nameEditText.setVisibility(View.GONE);
            continueButton.setEnabled(true);
        } else {
            nameTextView.setVisibility(View.GONE);
            needNameTextView.setVisibility(View.VISIBLE);
            nameEditText.setVisibility(View.VISIBLE);
            nameEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    continueButton.setEnabled(!TextUtils.isEmpty(charSequence));
                }

                @Override
                public void afterTextChanged(Editable editable) {}
            });
        }

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_go:{
                Intent intent = new Intent(getActivity(), ProtectsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                if(!isAuthorised){
                    intent.putExtra(ProtectsActivity.KEY_USERNAME, nameEditText.getText().toString().trim());
                }
                startActivity(intent);
            } break;
        }
    }
}
