package ru.fivecg.sensorlockscreen.protects;

import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ru.fivecg.sensorlockscreen.utils.FragmentHostActivity;

public class ProtectRecordActivity extends FragmentHostActivity {

    public static final String KEY_SENSOR_TYPE = "sensor_type";

    @Override
    protected Fragment createFragment() {
        return ProtectRecordFragment.newInstance(getIntent().getIntExtra(KEY_SENSOR_TYPE, -1));
    }
}
